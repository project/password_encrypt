<?php

/**
 * @file
 * A module to encrypt user password on client side.
 */

/**
 * Implements hook_help().
 */
function password_encrypt_help($path, $arg) {
  switch ($path) {
    case 'admin/help#password_encrypt':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module encrypts the user password on user form submission.
        It prevents the propagation of plain password over internet on user form submission.') . '</p>';
      $output .= '<h3>' . t('Dependencies:') . '</h3>';
      $output .= '<ol><li>' . t('OpenSSL extension.') . '</li>';
      $output .= '<li>' . t('CryptoJS library.') . '</li>';
      $output .= '<li>' . t('AES JSON formatter for CryptoJS.') . '</li></ol>';
      $output .= '<h3>' . t('Installation:') . '</h3>';
      $output .= '<ol><li>' . t('Download CryptoJS library from %crypto and extract it. Move rollups/aes.js to %path.', [
        '%crypto' => url('https://code.google.com/archive/p/crypto-js/downloads'),
        '%path' => 'sites/all/libraries/CryptoJS',
      ]) . '</li>';
      $output .= '<li>' . t('Download AES JSON formatter js for CryptoJS from %aes_json and copy to %path.', [
        '%aes_json' => url('https://github.com/brainfoolong/cryptojs-aes-php/blob/master/aes-json-format.js'),
        '%path' => 'sites/all/libraries/CryptoJS',
      ]) . '</li>';
      $output .= '<li>' . t('Install and Enable Password Encrypt module.') . '</li>';

      $output .= '<h3>' . t('Configuration:') . '</h3>';
      $output .= '<ol><li>' . t('Go to %path and change the passkey for encryption/decryption.', [
        '%path' => 'admin/config/people/password-encrypt',
      ]) . '</li>
      <li>Clear cache.</li>
      </ol>';

      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function password_encrypt_menu() {
  $items = [];
  $items['admin/config/people/password-encrypt'] = [
    'title' => 'Password encrypt settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => [
      'password_encrypt_settings_form',
    ],
    'access arguments' => [
      'password encrypt administrator',
    ],
    'type' => MENU_NORMAL_ITEM,
    'description' => 'Administer the pass key configuration for encryption and decryption of user forms.',
  ];

  return $items;
}

/**
 * Implements hook_permission().
 */
function password_encrypt_permission() {
  return [
    'password encrypt administrator' => [
      'title' => t('Administer password encrypt'),
    ],
  ];
}

/**
 * Generation of admin settings form.
 */
function password_encrypt_settings_form() {
  $form['#markup'] = '<strong>' . t('Provide default passkey for encryption and decryption.') . '</strong>';

  $form['password_encrypt_passkey'] = [
    '#type' => 'textfield',
    '#title' => t('Passkey for encryption/decryption'),
    '#default_value' => variable_get('password_encrypt_passkey'),
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}

/**
 * Implements hook_preprocess_page().
 */
function password_encrypt_preprocess_page($variables) {
  $password_encrypt_passkey = variable_get('password_encrypt_passkey');

  drupal_add_js([
    'password_encrypt' => [
      'password_encrypt_passkey' => $password_encrypt_passkey,
    ],
  ], [
    'type' => 'setting',
  ]);

  if (module_exists('password_encrypt') && module_exists('libraries')) {
    $password_encrypt_path = drupal_get_path('module', 'password_encrypt');
    $password_encrypt_library_path = libraries_get_path('CryptoJS');

    if (!$password_encrypt_library_path) {
      drupal_set_message(t('CryptoJS library is not available. Please install the library in %path', [
        '%path' => 'sites/all/libraries/CryptoJS',
      ]), 'error');
    }
    else {
      drupal_add_js($password_encrypt_path . '/js/password_encrypt.js');
      drupal_add_js($password_encrypt_library_path . '/aes.js');
      drupal_add_js($password_encrypt_library_path . '/aes-json-format.js');
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function password_encrypt_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'user_login' || $form_id == 'user_login_block' || $form_id == 'user_register_form' || $form_id == 'user_profile_form') {
    array_unshift($form['#validate'], 'password_encrypt_user_decrypt_password');
  }
}

/**
 * Callback: called to encrypt user password.
 */
function password_encrypt_user_decrypt_password($form, &$form_state) {
  $password_encrypt_passkey = variable_get('password_encrypt_passkey');
  $decrypted_pass = password_encrypt_crypto_js_aes_decrypt($password_encrypt_passkey, $form_state['values']['pass']);
  $form_state['values']['pass'] = $decrypted_pass;
}

/**
 * Helper function for CryptoJS AES encryption/decryption.
 *
 * @param string $passphrase
 *   Passkey for encryption/decryption.
 * @param string $jsonstring
 *   Encoded string.
 *
 * @return string
 *   Decoded string.
 */
function password_encrypt_crypto_js_aes_decrypt($passphrase, $jsonstring) {
  $jsondata = json_decode($jsonstring, TRUE);

  try {
    $salt = hex2bin($jsondata["s"]);
    $iv = hex2bin($jsondata["iv"]);
  }
  catch (Exception $e) {
    return NULL;
  }

  $ct = base64_decode($jsondata["ct"]);
  $concated_passphrase = $passphrase . $salt;
  $md5 = array();
  $md5[0] = md5($concated_passphrase, TRUE);
  $result = $md5[0];
  for ($i = 1; $i < 3; $i++) {
    $md5[$i] = md5($md5[$i - 1] . $concated_passphrase, TRUE);
    $result .= $md5[$i];
  }
  $key = substr($result, 0, 32);
  $data = openssl_decrypt($ct, 'aes-256-cbc', $key, TRUE, $iv);

  return json_decode($data, TRUE);
}

/**
 * Encrypt value to a cryptojs compatible json encoding string.
 *
 * @param string $passphrase
 *   Passkey for encryption/decryption.
 * @param string $value
 *   Encoded string.
 *
 * @return string
 *   Return encoded string data.
 */
function password_encrypt_crypto_js_aes_encrypt($passphrase, $value) {
  $salt = openssl_random_pseudo_bytes(8);
  $salted = '';
  $dx = '';
  while (strlen($salted) < 48) {
    $dx = md5($dx . $passphrase . $salt, TRUE);
    $salted .= $dx;
  }
  $key = substr($salted, 0, 32);
  $iv = substr($salted, 32, 16);
  $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, TRUE, $iv);
  $data = [
    "ct" => base64_encode($encrypted_data),
    "iv" => bin2hex($iv),
    "s" => bin2hex($salt),
  ];
  return json_encode($data);
}
